<h1>Word2Vec Implementation</h1>

<h2>Overview</h2>
<p>This project implements the Word2Vec model along with Stochastic Gradient Descent (SGD) optimizer. The Word2Vec model is trained to generate word embeddings from a given text corpus. The project includes the following components:</p>
<ul>
    <li><strong>word2vec.py</strong>: Implementation of Word2Vec model including methods for sigmoid, naive softmax loss and gradient, negative sampling loss and gradient, and skip-gram model.</li>
    <li><strong>sgd.py</strong>: Implementation of the SGD optimizer.</li>
    <li><strong>utils</strong>: Directory containing utility functions for gradient checking and other helper functions.</li>
    <li><strong>get_datasets.sh</strong>: Script to fetch the real data.</li>
    <li><strong>run.py</strong>: Main file to run and train word vectors.</li>
    <li><strong>word_vectors.png</strong>: Output, visualization of trained word vectors.</li>
</ul>

<h2>Instructions</h2>
<ol>
    <li>Clone the repository from GitLab.</li>
    <li>Navigate to the project directory.</li>
    <li>Ensure Python and necessary dependencies are installed.</li>
    <li>Run <code>python word2vec.py</code> to test the entire Word2Vec implementation.</li>
    <li>Run <code>python sgd.py</code> to test the SGD optimizer.</li>
    <li>Run <code>sh get_datasets.sh</code> to fetch the required datasets (Stanford Sentiment Treebank).</li>
    <li>Run <code>python run.py</code> to train word vectors using the SST dataset.</li>
</ol>

<h2>Results</h2>
<p>After running <code>run.py</code>, the training process will commence. Depending on the compute power, it may take few hours to complete 40,000 iterations.</p>
<p>Once finished, a visualization of the trained word vectors will be saved as <code>word_vectors.png</code> in your project directory.</p>

<h2>Additional Notes</h2>
    Feel free to experiment with hyperparameters and different datasets to further improve your word embeddings.
</html>
